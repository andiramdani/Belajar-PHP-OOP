<?php

	/**
	* Belajar Metode Set dan Get
	*/
	class Robot
	{
		//properti
		// var $nama = "ANDI RAMDANI";
		var $nama;
		var $tinggi;

		function __construct($nama){
			$this->nama = $nama;
		}

		//metode Set dan Get
		function SetNama($namaBaru){
			$this->nama = $namaBaru;
		}

		//metode Set dan Get
		function GetNama(){
			return $this->nama;
		}
		
	}

	/**
	* Balajar Pewarisan Kelas Objek
	*/
	class Hewan extends Robot
	{
		
		function __construct($nama)
		{
			$this->nama = $nama;
		}
	}

?>