Hasil yang kita dapat adalah:

Constructor dari class komputer
Constructor dari class laptop
Constructor dari class chromebook
Belajar OOP PHP
Destructor dari class chromebook
Destructor dari class laptop
Destructor dari class komputer
Dengan memanggil manual perintah parent::__construct() dan parent::__desctruct(), kita bisa menjalankan seluruh constructor dan destructor dari parent class.

Dalam tutorial belajar Object Oriented Programming (OOP) PHP kali ini, kita telah mempelajari cara kerja constructor dan destructor jika sebuah class diturunkan dari class lain (inheritance).

Constructor dan destructor parent class akan dijalankan jika child class tidak mendefenisikan constructor dan destructor sendiri. Namun jika child class juga memiliki constructor dan desctructor, maka kita harus memanggil constructor dan destructor parent class secara manual.