Kode diatas memiliki constructor dan destructor pada masing-masing class, mari kita lihat hasilnya:

Constuctor dari class chromebook
Belajar OOP PHP
Destructor dari class chromebook
Kemana constructor dan destructor class lainnya?

Di dalam PHP, ketika child class memiliki constructor dan destructor sendiri, maka PHP akan melewatkan constructor dan destructor parent class, kasus ini disebut dengan Overridden Constructor dan Overridden Destructor. Karena di dalam contoh kita class chromebook memiliki constructor dan destructor, maka constructor dan destructor class induknya tidak dijalankan.

Bagaimana jika kita ingin constructor dan destructor parent class tetap dijalankan?

Solusinya, kita harus memanggil constructor dan destructor parent class secara manual dengan Scope Resolution Operator, yakni: parent::__construct() dan parent::__desctruct().