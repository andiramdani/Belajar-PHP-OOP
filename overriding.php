<?php

	/**
	* Belajar Metode Set dan Get
	*/
	class Robot
	{
		
		//properti
		// var $nama = "ANDI RAMDANI";
		var $nama;
		
		function __construct($nama){
			$this->nama = $nama;
		}

		//metode Set
		function SetNama($namaBaru){
			$this->nama = $namaBaru;
		}

		//metode Get
		function GetNama(){
			return $this->nama;
		}
		
	}

	/**
	* Balajar Pewarisan Kelas Objek
	*/
	class Hewan extends Robot
	{
		
		function __construct($nama)
		{
			$this->nama = $nama;
		}

		//metode Get
		function GetNama(){
			// echo Robot::GetNama().'<br>';
			echo Parent::GetNama().'<br>';
			return "HI ". $this->nama;
		}
	}

?>