Pada kode diatas, saya membuat class komputer dengan construktor dan destructor. Class komputer kemudian diturunkan kepada class laptop, kemudian diturunkan kembali kepada class chromebook. Baik class laptop maupun class chromebook tidak memiliki property maupun method. Class chromebook inilah yang akan kita instansiasi kedalam objek $gadget_baru.

Ketika program itu dijalankan, berikut adalah hasil yang didapat:

Constuctor dari class komputer
Belajar OOP PHP
Destructor dari class komputer