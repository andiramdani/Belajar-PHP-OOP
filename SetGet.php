<?php

	/**
	* Belajar Metode Set dan Get
	*/
	class Robot
	{
		//properti
		// var $nama = "ANDI RAMDANI";
		var $nama;
		var $tinggi;

		//metode Set dan Get
		function SetNama($nama){
			$this->nama = $nama;
		}

		//metode Set dan Get
		function GetNama(){
			return $this->nama;
		}
		
	}

?>