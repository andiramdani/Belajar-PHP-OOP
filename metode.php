<?php

	/**
	* Belajar Kelas dan Metode PHP OOP
	*/
	class Robot
	{
		//properti
		// var $nama = "ANDI RAMDANI";
		var $nama;
		var $tinggi;

		//metode
		function ambilNama ($nama){
			return $this->nama = $nama;
		}
		
	}

	//membuat objek dari kelas robot
	$test = new Robot();
	echo $test->ambilNama("Andi Ramdani");

?>