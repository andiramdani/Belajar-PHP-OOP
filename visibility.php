<?php

	/**
	* Belajar Metode Set dan Get
	*/
	class Robot
	{	
		// public, Protected, Private :

		// public : bisa di akses oleh semua kelas dan pewarisannya
		// Protected : hanya bisa dipakai oleh kelas utama dan class pewarisannya
		// private : hanya bisa dipakai oleh kelas itu sendiri

		//properti
		// var $nama = "ANDI RAMDANI";
		
		protected $nama;
		// private $nama;
		
		function __construct($nama){
			$this->nama = $nama;
		}

		//metode Set
		function SetNama($namaBaru){
			$this->nama = $namaBaru;
		}

		//metode Get
		function GetNama(){
			return $this->nama;
		}
		
	}

	/**
	* Balajar Pewarisan Kelas Objek
	*/
	class Hewan extends Robot
	{
		
		function __construct($nama)
		{
			$this->nama = $nama;
		}

		//metode Get
		function GetNama(){
			// echo Robot::GetNama().'<br>';
			echo Parent::GetNama().'<br>';
			return "HI ". $this->nama;
		}
	}

?>