Tutorial Belajar OOP PHP Part 13: Pengertian Konstanta Class dalam Pemrograman Objek09 Oct 14 | Andre | Tutorial PHP | 3 Comments
Dalam tutorial belajar object oriented programming PHP kali ini, kita akan mempelajari pengertian konstanta class. Cara pengaksesan konstanta class dalam PHP, mirip dengan cara mengakses static property yang telah kita pelajari dalam tutorial sebelumnya.

Pengertian Konstanta Class
Konstanta Class atau class constant adalah konstanta yang berada di dalam class. Selain memiliki property dan method, PHP juga membolehkan kita menggunakan konstanta (constant) di dalam class.

Sebagaimana sifat konstanta reguler, class constant juga tidak bisa diubah nilainya ketika sudah didefenisikan. Untuk membuat class constant di dalam PHP, kita menggunakan perintah: const.


Hampir semua sifat konstanta reguler juga sama di dalam konstanta class. Pembahasan tentang konstanta reguler telah kita bahas dalam Tutorial PHP Dasar: Pengertian Konstanta dan Cara Penulisan Konstanta PHP
Berikut adalah contoh kode program pembuatan constanta di dalam class:

class nama_class {
   const NAMA_KONSTANTA = nilai_konstanta;
}

Penulisan nama konstanta dengan huruf besar bukan keharusan, namun lebih kepada kebiasaan programmer PHP agar mudah dibedakan dengan variabel yang umumnya ditulis dengan huruf kecil.

Di dalam PHP, class constant seolah-olah berprilaku sebagai static property. Class constant juga terikat kepada class, bukan objek. Oleh karena itu, untuk mengakses nilai konstanta, kita menggunakan operator yang sama seperti static property, yakni menggunakan double colon ‘::’.

Jika kita memiliki class laptop dan konstanta MERK, maka cara mengaksesnya adalah sebagai berikut:

laptop::MERK;
Cara Penulisan Konstanta Class dalam PHP
Untuk melihat cara penulisan dan penggunaan konstanta class, kita akan langsung menggunakan kode program. Berikut adalah contoh class laptop dengan sebuah konstanta DOLLAR:

<?php
// buat class laptop
class laptop {
   // buat konstanta
   const DOLLAR = '12000';
}
 
// panggil konstanta class
echo "Harga dollar saat ini = Rp. ".laptop::DOLLAR;
// hasil: Harga dollar saat ini = Rp. 12000
?>
Perhatikan bahwa untuk mengakses class constant DOLLAR milik class laptop, kita menggunakan perintah laptop::DOLLAR.

Selain mengakses konstanta dengan menggunakan nama class, PHP juga memiliki cara lain, yakni dengan mengaksesnya dari objek. Fitur ini hanya bisa digunakan untuk PHP versi 5.3 keatas. Berikut contohnya:

<?php
// buat class laptop
class laptop {
   // buat konstanta
   const DOLLAR = '12000';
}
 
// buat objek dari class laptop (instansiasi)
$laptop_baru = new laptop();
 
// panggil konstanta class
echo "Harga dollar saat ini = Rp ".$laptop_baru::DOLLAR;
// hasil: Harga dollar saat ini = Rp. 12000
?>
Dalam kode diatas, kita mengakses nilai kontanta class dari objek $laptop_baru menggunakan perintah $laptop_baru::DOLLAR.

PHP versi 5.3 keatas juga membolehkan pemanggilan property dengan nama class yang berada di dalam variabel. Berikut contohnya:

<?php
// buat class laptop
class laptop {

   // buat konstanta
   const DOLLAR = '12000';
}
 
// buat variabel dengan nama class
$nama = "laptop";
 
// panggil konstanta class
echo "Harga dollar saat ini = Rp. ".$nama::DOLLAR;
// hasil: Harga dollar saat ini = Rp. 12000
?>

Pada kode program diatas, saya tidak menggunakan objek, tetapi membuat variabel $nama dan memberikannya nilai laptop. Karena nama class kita juga adalah laptop, maka PHP membolehkan pemanggilan kosntanta DOLLAR dengan $nama::DOLLAR. Nama variabel yang digunakan boleh bebas, selama nilainya cocok dengan nama class tempat konstanta itu berada.

Cara Mengakses Konstanta Class dari dalam Class itu Sendiri
Untuk mengakses class constant dari dalam class itu sendiri, PHP menggunakan cara yang sama dengan static property, yaitu dengan perintah self::nama_konstanta. Berikut contohnya:

<?php
// buat class laptop
class laptop {
 
   // buat konstanta
   const DOLLAR = '12000';
 
   // buat method
   public function beli_laptop($harga) {
     return "Beli Komputer Baru, Rp. ".$harga*self::DOLLAR;
   }
}
 
// buat objek dari class laptop (instansiasi)
$laptop_baru=new laptop();
 
echo $laptop_baru->beli_laptop(400);
// hasil: Beli Komputer Baru, Rp. 4800000
?>

Saya membuat class laptop dengan sebuah method beli_laptop(). Method beli_laptop() digunakan untuk menghitung harga laptop dengan mengalikan konstanta class DOLLAR dengan parameter $harga. Perhatikan bahwa kita mengakses class constant dengan perintah self::DOLLAR.

Cara Mengakses Konstanta Class milik Parent Class
Pewarisan class (class inheritance) dari sebuah class kedalam class lain, juga akan menurunkan konstanta. Jika kebetulan class yang diturunkan (child class) memiliki nama konstanta yang sama dengan parent class, konstanta tersebut akan ‘tertimpa’.

PHP menggunakan operator parent::nama_konstanta untuk mengakses konstanta milik parent class.

Agar lebih mudah, berikut adalah contoh kode program penggunaan operator parent::nama_konstanta:

<?php
  // buat class komputer
  class komputer {
     // buat konstanta class komputer
     const DOLLAR = '11000';
  }

  // turunkan class komputer ke class laptop
  class laptop extends komputer {
     // buat konstanta class laptop
     const DOLLAR = '12000';
   
     // buat method dengan konstanta class komputer
     public function beli_komputer($harga){
       return "Beli Komputer Baru, Rp .".$harga*parent::DOLLAR;
     }
   
     // buat method dengan konstanta class laptop
     public function beli_laptop($harga){
       return "Beli Komputer Baru, Rp .".$harga*self::DOLLAR;
     }
  }
   
  // buat objek dari class laptop (instansiasi)
  $laptop_baru=new laptop();
   
  echo $laptop_baru->beli_laptop(400);
  echo "<br />";
  echo $laptop_baru->beli_komputer(400);
?>

Saya membuat konstanta DOLLAR di dalam class komputer. Class komputer kemudian diturunkan ke dalam class laptop. Di dalam class laptop, saya mendefenisikan kembali konstanta DOLLAR. Karena kedua konstanta ini memiliki nama yang sama, maka saya harus menggunakan perintah parent::DOLLAR untuk memanggil konstanta DOLLAR miliki class komputer.

Dalam tutorial OOP PHP kali ini kita telah mempelajari tentang pengertian konstanta class dan cara penggunaan konstanta class dalam PHP. Walaupun konstanta class jarang digunakan di dalam pemograman umum, namun fitur yang ditawarkan mungkin bisa membantu untuk penyelesaian kasus-kasus tertentu.