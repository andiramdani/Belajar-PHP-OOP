Pada kode diatas, saya membuat class komputer dengan sebuah static method beli_komputer(). Method ini memiliki hak akses protected, sehingga hanya bisa diakses dari dalam class itu sendiri atau dari dalam class turunan.

Class komputer kemudian ‘diturunkan’ kepada class laptop. Di dalam class laptop, saya membuat dua buah static method. Static method beli_laptop() di set dengan hak akses private, sehingga tidak bisa diakses dari luar class laptop.

Dalam method beli_semua(), saya memanggil method beli_komputer() milik class komputer menggunakan perintah parent::beli_komputer(). Ini adalah cara pemanggilan static method milik parent class. Kemudian masih di dalam method beli_semua(), saya memanggil method beli_laptop() dengan perintah self::beli_laptop(), karena method ini ada di dalam class leptop itu sendiri.

Untuk menguji apakah method beli_semua() sukses dijalankan, saya kemudian memanggilnya dengan perintah laptop::beli_semua().

Perhatikan juga pada bagian komentar di akhir kode diatas. Jika kita mencoba memanggil method laptop::beli_laptop(), PHP akan mengeluarkan error karena method beli_laptop() memiliki hak akses private, sehingga tidak bisa diakses dari luar class.